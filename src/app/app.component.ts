import { OnInit, AfterViewInit, Component, ViewEncapsulation, ViewChild, HostListener, ElementRef } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import * as THREE from 'three';
import { Particle, EventService } from '../services/event.service';
import * as d3 from 'd3';

import { Options } from 'ng5-slider';
import { NgForm, FormGroup, FormControl } from '@angular/forms';

import { interval } from 'rxjs';

declare var JSROOT: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {
  title = 'example-docker';

  // -------  SCENE THREE.JS ----------------------------------------------------------------

  // scene properties
  // private canvas: HTMLCanvasElement;
  renderer: THREE.WebGLRenderer;
  camera: THREE.PerspectiveCamera;
  scene: THREE.Scene;
  light: THREE.AmbientLight;
  controls: OrbitControls;

  cube: THREE.Mesh;

  geomsize = 400;
  dummy: any;
  box3: THREE.Box3;

  // canvas
  @ViewChild('canvas', { static: true })
  canvasRef: ElementRef;
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  createScene() {
    // create the scene
    this.scene = new THREE.Scene();

    // camera
    this.camera = new THREE.PerspectiveCamera(
      45, window.innerWidth / window.innerHeight, 1, this.geomsize * 5
    );
    this.camera.position.y = this.geomsize;
    this.scene.add(this.camera);

    // Lights
    this.light = new THREE.AmbientLight(0x404040);
    this.scene.add(this.light);

    const light = new THREE.DirectionalLight(0xffffff);
    light.position.set(0, 1, 0);
    this.scene.add(light);

    // Renderer
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
    this.renderer.setClearColor(0x000000); // e1e0e0
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    // controls
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target.set(0.0, 0.0, 7.0);
    this.controls.maxPolarAngle = 0.5 * Math.PI;
    this.controls.autoRotate = false;
    this.controls.update();

    // cube
    const geometry = new THREE.BoxGeometry(50, 50, 50);
    const material = new THREE.MeshBasicMaterial({ color: 0xFF0000 });
    this.cube = new THREE.Mesh(geometry, material);
    // this.scene.add(this.cube);

    const material2 = new THREE.MeshLambertMaterial({ side: THREE.DoubleSide });
    this.dummy = new THREE.Mesh(new THREE.TetrahedronGeometry(75, 0), material2);
    // this.scene.add( this.dummy );
  }

  @HostListener('window:resize', ['$event'])
  // onWindowResize(event): the render and camera resize the contents of the canvas
  onWindowResize(event: any) {
    this.canvas.style.width = '100%';
    this.canvas.style.height = '100%';

    this.camera.aspect = this.canvas.clientWidth / this.canvas.clientHeight;
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.camera.updateProjectionMatrix();
  }

  // LOOP
  /* startRenderingLoop(): create a loop that causes the renderer to draw
  the scene every time the screen is refreshed*/
  startRenderingLoop() {
    const component: AppComponent = this;

    (function render() {
      requestAnimationFrame(render);
      // JSROOT.GEO.produceRenderOrder(this.scene, this.camera.position, 'box');
      component.renderer.render(component.scene, component.camera);
    }());
  }



  // -------------   JSROOT  --------------------------------------------------------------------

  private add_geometry(obj: any) {
    // options for building three.js model
    const opt = { numfaces: 100000, numnodes: 1000 };

    // function demonstrate creation of three.js model
    const obj3d = JSROOT.GEO.build(obj, opt);              // <----------- 'ERROR: JSROOT.GEO undefined'

    if (!obj3d) { return; }

    this.scene.remove(this.dummy);
    this.scene.add(obj3d);

    const box3 = new THREE.Box3().setFromObject(obj3d);

    // this.geomsize = this.box3.getSize().length();
    this.geomsize = 600;

    this.camera.far = this.geomsize * 5;
    this.camera.updateProjectionMatrix();
  }

  private loadDetector() {
//    function add_geometry(obj: any) {
//      // options for building three.js model
//      const opt = { numfaces: 100000, numnodes: 1000 };
//
//      // function demonstrate creation of three.js model
//      const obj3d = JSROOT.GEO.build(obj, opt);              // <----------- 'ERROR: JSROOT.GEO undefined'
//
//      if (!obj3d) { return; }
//      console.log('SCENE', this.scene);
//      this.scene.remove(this.dummy);
//      this.scene.add(obj3d);
//
//      const box3 = new THREE.Box3().setFromObject(obj3d);
//
//      this.geom_size = this.box3.getSize().length();
//
//      this.camera.far = this.geom_size * 5;
//      this.camera.updateProjectionMatrix();
//    }

    // const filename = JSROOT.GetUrlOption('file') || 'https://root.cern/js/files/geom/simple_alice.json.gz';

    const filename = JSROOT.GetUrlOption('file') || 'https://root.cern/js/files/geom/lhcb.json.gz';

    // const filename = '/afs/cern.ch/user/m/mablanch/workspace/detector/lhcbfull.root';

    if (filename.indexOf('.root') > 0) {
      // load geometry from ROOT file
      const itemname = JSROOT.GetUrlOption('item') || 'Geometry';
      JSROOT.OpenFile(filename, (file: any) => {
        file.ReadObject(itemname, this.add_geometry);
      });
    } else {
      // load geometry from JSON as object
      // JSROOT.NewHttpRequest(filename, 'object', this.add_geometry).send();
      JSROOT.NewHttpRequest(filename, 'object', (obj: any) => {
        // options for building three.js model
        const opt = { numfaces: 100000, numnodes: 1000 };
        // function demonstrate creation of three.js model
        const obj3d = JSROOT.GEO.build(obj, opt);              // <----------- 'ERROR: JSROOT.GEO undefined'
        if (!obj3d) { return; }
        this.scene.remove(this.dummy);
        this.scene.add(obj3d);
        const box3 = new THREE.Box3().setFromObject(obj3d);

        //this.geomsize = box3.getSize().length();

//        let tmp_vec3: THREE.Vector3;
//        console.log(box3);
//        box3.getSize(tmp_vec3);
//        console.log(tmp_vec3);
//        this.geomsize = tmp_vec3.length();

        // this.geomsize = 600;
        this.camera.far = this.geomsize * 5;
        this.camera.updateProjectionMatrix();
      }).send();
    }

    // ----------- end of reading geometry object


    // dummy object, seen in the beginning
    const material = new THREE.MeshLambertMaterial({ side: THREE.DoubleSide });
    this.dummy = new THREE.Mesh(new THREE.TetrahedronGeometry(75, 0), material);
    this.scene.add(this.dummy);
  }

  // ------ CREATE EVENT ------------------------------------------------------
  alllines: THREE.Object3D;
  private theevent: Particle[];
  theparticle: Particle;

  // CREATE EVENT 
  /* createEvent(event: Particle[]): draw tracks from json data */
  createEvent(event: Particle[]){

    this.alllines = new THREE.Object3D; // allocation of new 3D object which will contain all tracks
    
    // parse particle
    for ( let i in event ) {
      var track = event[i].trajectory; // fetch coordinates
      var name = event[i].particleId; // fetch particle name
      var points = [];
      for ( let j in track ) {
        var point = track[j]; // coordinates x, y, z of a point
        points.push( new THREE.Vector3(1.0e-3*point[0], 1.0e-3*point[1], 1.0e-3*point[2]) ); // THREE.Vector3 is here a point in 3D space
      }
      var spline = new THREE.CatmullRomCurve3(points); // 3D spline curve from a series of points
      var vertex = spline.getPoints(200);
      var geometryLine = new THREE.BufferGeometry().setFromPoints( vertex );
            
      var line;
      // create final object to add to the scene 
      if (name=="pi+")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x32CD32, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="pi-")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4B0082, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="K+" )
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4169E1, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="K-")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0xFF4500, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="D0")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x999999, opacity: 0, transparent: true, linewidth: 0.1 } ), THREE.LineStrip );
      else 
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x999999, linewidth: 1 } ), THREE.LineStrip );

      this.alllines.add(line);
    }
   this.scene.add( this.alllines );
  }


  // PRINCIPAL FUNCTION  --------------------------------------------------------

  ngOnInit() {

    /*const waitForGeoInit = interval(100).subscribe(() => {
      if (JSROOT.GEO) {
        waitForGeoInit.unsubscribe();
        this.createScene();
        this.loadDetector();
        this.startRenderingLoop();
      }
    });*/






  }


  //------------------------ SLIDER -----------------
  minValue: number = 10;
  maxValue: number = 90;
  options: Options = {
    floor: 0,
    ceil: 100,
    step: 10,
    showTicks: true
  };
}


